﻿using System;

namespace Day_7.Model
{
    public class TransferViewModel: EventArgs
    {
       
        public int Amount { get; set; }

        public string SenderAccount { get; set; }
        public string DestAccount { get; set; }
    }
}